# pdfdrawover

Write and draw over a pdf file from the command line.
This allows for programmatic changes to pdf files.

Some examples (see man page for details):

- print the page number at the bottom-right corner, in the form "1 of 10":
``
pdfdrawover file.pdf moveto [-50,-10] print %pagenumber print " of 10"
``

- write two strings in font 20 along the left edge of the first page:
``
pdfdrawover file.pdf page 1 fontsize 20 moveto [30,-100] rotate -90 print "something!" print " and something else"
``

- draw a rectangle around each page, leaving a larger margin on the left of odd pages and on the right of even pages:
``
pdfdrawover file.pdf page odd rectangle [40,10--10,-10] page even rectangle [10,10--40,-10]
``

- hide the top of the page by drawing a white rectangle over it (note that the text underneath is not removed from the page, and can still be retrieved by text extractors such as pdftoroff):
``
pdfdrawover file.pdf color white filledbox [50,50--50,150]
``

- print two lines at the top of page 1:
``
pdfdrawover file.pdf page 1 moveto [50,50] fontsize 12 print "this is an heading" print %newline print "second heading"
``

- insert an header and trailer page:
``
pdfdrawover file.pdf page 1 emptypage print "header" page last+1 emptypage print "trailer"
``

- create a document of three pages:
``
pdfdrawover none pagesize a4 emptypage start print "this is a pdf file" next print "another page" print %newline print "and now..." next moveto [100,100] print "the end" next end
``

- put every pair of a4 input pages into one output page:
``
pdfdrawover file.pdf pagesize [842,595] emptypage moveto [0,0] scale 1/sqrt2 paste moveto [421,0] scale 1/sqrt2 paste
``

- exchange odd and even pages on a document containing an even number of pages:
``
pdfdrawover file.pdf page odd inputpage +1 copy inputpage -2 page even copy inputpage +1
``


# scripts

## ``pdfheader``
add an header page to a pdf file

![an header page added to a sequence of pages](/figures/header.png)

## ``pdfleftedge``
print along the left edge of all pages

![something written along the left edge](/figures/leftedge.png)

## ``pdfpagenumbers``
add page numbers to a pdf file

![page numbers added to all pages](/figures/numbers.png)

## ``pdfroll``
bottom of a page and top of next in each page

![bottom of a page and top of next in one output page](/figures/roll.png)

## ``pdfrotate``
rotate a document (assumed a4)

![an input page becomes an output page, rotated](/figures/rotate.png)

## ``pdfselect``
select a range of pages

![two pages out of four selected](/figures/select.png)

## ``pdfsimplex``
interleave a pdf with empty pages

![blank page inserted between each consecutive ones](/figures/simplex.png)

## ``pdfcut``
cut margins of a pdf file

## ``pdfsplit``
split a pdf file into its individual pages

![each page becomes a separate file](/figures/split.png)

## ``pdftile``
split an a4 page in two sheet

![a single page makes two output pages](/figures/tile.png)

## ``pdftwoinone``
two a4 pages in one

![two input pages in a single output page](/figures/twoinone.png)

## ``pdfwatermark``
add a diagonal watermark

![print something in diagonal](/figures/watermark.png)

## ``pdftranslate``
translate all pages

![a page translated](/figures/translate.png)

## ``pdfcenter``
place the pages of a file at the center of larger pages

![a page centered into a larger page](/figures/center.png)

