PROGS=pdfdrawover

CFLAGS+=-g -Wall -Wextra -Wformat -Wformat-security
CFLAGS+=${shell pkg-config --cflags poppler-glib}
LDLIBS+=${shell pkg-config --libs poppler-glib}

all: ${PROGS}

install: all
	mkdir -p ${DESTDIR}/usr/bin
	cp pdfdrawover ${DESTDIR}/usr/bin
	mkdir -p ${DESTDIR}/usr/share/man/man1
	cp pdfdrawover.1 ${DESTDIR}/usr/share/man/man1

pdfdrawover: pdfname.o paper.o

clean:
	rm -f *.o ${PROGS}

