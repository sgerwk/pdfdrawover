/*
 * pdfdrawover.c
 *
 * draw over a pdf file
 *
 * pdfdrawover file.pdf box [150,150+50,50] \
 *                  color [0.2,0.8,0.3] \
 *                  box [250,200-280,250] \
 *                  box [-50,-50--10,-10] \
 *                  moveto [400,200] \
 *                  print abcd \
 *                  moveto [500,800] \
 *                  print %pagenumber \
 *                  print " of 10" \
 *                  page even \
 *                  box [200,200+50,50]	\
 *                  page last-1 \
 *                  filledbox [300,200+50,50] \
 *                  output "somename.pdf" \
 *                  count
 *
 * commands:
 *	help
 *	paper
 *	count
 *	size
 *	output filename.pdf
 *	nooutput
 *	page (n|all|odd|even|last|last-n|last+n)
 *	pagerotate
 *	pagesize [x,y]
 *	emptypage
 *	copypage
 *	discard
 *	inputpage (n|+n|-n|last|last-n)
 *	end
 *	nop
 *	reset
 *	rotate
 *	scale
 *	clip [x,y+width,height]
 *	clip [x,y-xf,yf]
 *	color [r,g,b]
 *	fontsize n
 *	fontface name
 *	fontslant (normal|oblique|italic)
 *	fontweight (normal|bold)
 *	box [x,y+width,height]
 *	box [x,y-xf,yf]
 *	filledbox [x,y+width,height]
 *	filledbox [x,y-xf,yf]
 *	image file.jpg
 *	moveto [x,y]
 *	print (string|%pagenumber)
 *	extents string
 *	printfile file.txt
 *	stdout string
 *	comment string
 *	paste
 *	pastesame
 *	pasteend
 *	stroke
 *
 * negative coordinates are from the bottom-right corner; this is for x,y of
 * moveto and box and for xf,yf of the [x,y-xf,yf] form of box
 */

/*
 * todo:
 *
 * - macros: a name and an array of strings (commands to be passed)
 *   selected by "macro name" or by argv[0]==pdfname
 * - allow more then one input file
 * - allow png file(s) as input
 * - when input file cannot be opened, tell why it is being read
 * - "page start+n": matches when input page n is shown; in the first loop find
 *   the list of input pages that are shown in this output page (possibly
 *   none); in the second, match all start+n and last-n pages; this also allow
 *   for "page input", to match exactly when at least an input page is shown in
 *   the ouptut page
 * - widechar, utf-8 text
 * - set line thickness
 * - maintain stack of command files, print stack in case of parsing error
 * - lineto (example: cross out a given page)
 * - line numbers (automatic detection of lines)
 * - calculate text extents with the current font etc.
 * - variables in addition to %pagenumber: boundingbox coordinates, lines,
 *   textarea, page size; allow their use in coordinates; this way,
 *   %pagewidth-20 replaces -20
 * - also use text text extents as variables; this allows centering a string
 * - use lua for expression evaluation, if available;
 *   otherwise tinyexpr (from github); otherwise disallow expressions
 * - page ranges, like 2-10
 * - use arbitrary conditions in page, with an expression evaluator where
 *   %page and %last are variables; for example, '%page==%last && odd(%last)'
 *   or '%page==%last-1' or '%page!=1' or '%page>=2 && %page<=%last'; it is
 *   possible because of how the page specification is used: the current page
 *   is matched with each "page" command in the list; this requires only
 *   something that generates a boolean given the current page number; it allow
 *   differentiating between %inpage and %outpage; if the expression evaluates
 *   to a number and not a boolean it is interpreted as "%outpage==number"
 * - "page last+1 inputpage 1" causes an infinite loop; the seemingly
 *   equivalent "page 4 inputpage 1" on a 3-page document does not; there is no
 *   obvious way to detect a loop, as for example "page 1 inputpage 4 page
 *   last+1 inputpage 2 page 3 end" is not an infinite loop
 */

/*
 * input and output
 * ----------------
 *
 * depending on the commands, the input file may not be read or the output file
 * not be generated; in particular, input is not read if all pages are created
 * via emptypage and no paste is executed; output is not generated if all
 * commands are count, size or cropbox
 *
 * to realize this, input is only opened or read when a command needs it;
 * output is only created when a command draws something; the following
 * functions open the input or the output if not already, so that they can be
 * called whenever input or output is necessary, even multiple times
 *
 * ensureinput()
 *	open the input if not already; upon opening, read the number of input
 *	pages and increase the number of output pages by this amount; called
 *	directly only when the number of input pages is needed; otherwise, it
 *	is called via ensurereadpage()
 *
 * ensurereadpage()
 *	ensure that the current input page is read: open the input file if not
 *	already and read the page if it has not been read yet; to force reading
 *	the next page, close the current one with closepage(&in); this function
 *	is called directly by the commands that need an input page but either
 *	produce no output (size and cropbox) or need to force reading the next
 *	page (paste); the other commands call it via ensureoutput()
 *
 * ensureoutput()
 *	open the output file if not already, create a new output page if not
 *	already; if the page is not to be created empty (out->newpage), read
 *	the next input page and copy it to the new output page
 *
 * this mechanism requires that the input and output pages are closed at the
 * end of each iteration of the page loop; at the next iteration, the above
 * functions recognize that the input and the output page do not exists when
 * called the first time; this way, they read the next input page if and when
 * input is first requested and create the next output page if and when needed
 *
 * also necessary is out.draw, which stores whether something has been actually
 * written; otherwise, settings operations like "fontsize 20", which requires
 * an output to operate, would generate an output page
 */

/*
 * note: pushend
 *
 * when the input document is opened, the output size is increased by its
 * number of pages; this makes the output ends when the input ends, and is
 * correct in the default case, when each input page goes to one output page
 *
 * the emptypage and paste comands may violate this rule, in that an output
 * page may contain no input page (emptypage with no paste) or more than one
 * (emptypage with two or more paste, or no emptypage and at least a paste)
 *
 * when this happens, the ending page becomes incorrect, together with all
 * last-n or last page specifiers; they are fixed by determining the number of
 * input pages that will be used for the current output page; the ending page
 * is then decreased by this number minus one; before doing this, the pushend()
 * function fixes the last-n and last pages by changing their diff field
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#include <poppler.h>
#include <cairo.h>
#include <cairo-pdf.h>

#include "pdfname.h"
#include "paper.h"

/*
 * verbosity
 */
int verbose;
#define VERBOSE_NONE 0
#define VERBOSE_COMMAND 1
#define VERBOSE_EXECUTED 2
#define VERBOSE_PARSE 3

/*
 * command argument from cmdline
 */
#define COMMANDERROR							\
	parsing->usage = TRUE;						\
	parsing->opterror = TRUE;					\
	return 0;
#define COMMANDARG(command, argument)					\
	if (arg == NULL) {						\
		printf(command " command, but no " argument " given\n");\
		COMMANDERROR						\
	}								\
	res = 2;

/*
 * operations only available from certain cairo versions on
 */

#if CAIRO_VERSION < CAIRO_VERSION_ENCODE(1, 16, 0)
#pragma message("WARNING: links and destinations not supported")
#define cairo_tag_begin(a, b, c)					\
	printf("WARNING: links and destinations not supported "		\
	       "in the installed cairo version\n");
#define cairo_tag_end(a, b) {}
#endif

/*
 * input structure: the input file and its current open page, if any
 */
struct input {
	char *filename;
	PopplerDocument *doc;
	PopplerPage *page;
	gdouble width;
	gdouble height;
	gboolean end;	// a page was requested but the document is finished
	int loaded;	// page currently loaded
	int inpage;	// next input page to be read
	int intot;	// number of pages in input document
};

/*
 * output structure: the output file and its page being written, if any
 */
struct output {
	char *filename;
	cairo_surface_t *surface;
	cairo_t *cr;
	gboolean rotate;
	int width;
	int height;
	char *fontface;
	cairo_font_slant_t fontslant;
	cairo_font_weight_t fontweight;
	int fontsize;
	gdouble startlinex;	// position, for printing text
	gdouble startliney;
	gdouble currentlinex;
	gdouble currentliney;
	gboolean emptypage;	// create empty page, do not copy input page
	gboolean copypage;	// copy input page on output first
	gboolean drawn;		// something drawn on page
	gboolean link;		// unfinished link
	gboolean end;		// output is finished: do not emit this page
	int outpage;
	int outtot;
};

/*
 * list of commands
 */
enum cairo_command_type {
	command_nop = 0,
	command_page,
	command_pagerotate,
	command_pagesize,
	command_emptypage,
	command_copypage,
	command_end,
	command_discard,
	command_inputpage,
	command_size,
	command_cropbox,
	command_verbose,
	command_color,
	command_reset,
	command_rotate,
	command_scale,
	command_clip,
	command_linewidth,
	command_fontsize,
	command_fontface,
	command_fontslant,
	command_fontweight,
	command_moveto,
	command_rectangle,
	command_filledrectangle,
	command_image,
	command_extents,
	command_print,
	command_printfile,
	command_link,
	command_destination,
	command_stdout,
	command_comment,
	command_paste,
	command_pastesame,
	command_pasteend,
	command_stroke
};

/*
 * command names
 */
char *command_name[] = {
	"nop",
	"page",
	"pagerotate",
	"pagesize",
	"emptypage",
	"copypage",
	"end",
	"discard",
	"inputpage",
	"size",
	"cropbox",
	"verbose",
	"color",
	"reset",
	"rotate",
	"scale",
	"clip",
	"linewidth",
	"fontsize",
	"fontface",
	"fontslant",
	"fontweight",
	"moveto",
	"rectangle",
	"filledrectangle",
	"image",
	"extents",
	"print",
	"printfile",
	"link",
	"destination",
	"stdout",
	"comment",
	"paste",
	"pastesame",
	"pasteend",
	"stroke"
};

/*
 * an input or output page, including +n, -n, last, last-n and last+n
 */
struct cairo_page {
	int number;	// or a cairo_pagetype element
	union {
		int diff;	// only if number is page_current or page_last
		int actual;	// actual page number of a next command
	};
};

/*
 * special page values for page->number
 */
enum cairo_pagetype {
	page_all = -1,
	page_odd = -2,
	page_even = -3,
	page_current = -4,	// page->diff	is difference from current page
	page_last = -5,		// page->diff	is difference from last page
	page_next = -6		// page->actual	is -1 or actual page number
};

struct cairo_point {
	gdouble x;
	gdouble y;
};

struct cairo_color {
	double red;
	double green;
	double blue;
	double alpha;
};

struct cairo_rectangle {
	gdouble x;
	gdouble y;
	gdouble height;
	gdouble width;
};

struct cairo_command {
	enum cairo_command_type type;
	enum cairo_arg {ARG_NONE, ARG_INT, ARG_DOUBLE, ARG_STRING,
			ARG_PAGE, ARG_POINT, ARG_COLOR, ARG_RECTANGLE} arg;
	union {
		int integer;
		double d;
		char *string;
		struct cairo_page *page;
		struct cairo_point *point;
		struct cairo_color *color;
		struct cairo_rectangle *rectangle;
	};
};

/*
 * page size
 */
struct {
	char *name;	int x;	int y;
} pagesize[] = {
	{"a4",		595,	842},
	{"letter",	612,	792},
	{NULL,		0,	0}
};

/*
 * colors
 */
#define RANDOM 1.1
struct {
	char *name;	double red;	double green;	double blue;
} color[] = {
	{"black",	0.0,		0.0,		0.0},
	{"white",	1.0,		1.0,		1.0},
	{"random",	RANDOM,		RANDOM,		RANDOM},
	{NULL,		0.0,		0.0,		0.0}
};

/*
 * print a page
 */
void print_page(struct cairo_page *page) {
	switch (page->number) {
	case page_all:
		printf("all");
		break;
	case page_odd:
		printf("odd");
		break;
	case page_even:
		printf("even");
		break;
	case page_current:
		printf("%d from current", page->diff);
		break;
	case page_last:
		printf("%d from last", page->diff);
		break;
	case page_next:
		if (page->actual == -1)
			printf("next");
		else
			printf("next = page %d", page->actual + 1);
		break;
	default:
		printf("%d", page->number + 1);
	}
}

/*
 * print a command
 */
void print_command(struct cairo_command *command, char * label, char *note) {
	printf("%s%s", label ? label : "", label ? " " : "");
	printf("%s ", command_name[command->type]);
	switch (command->arg) {
	case ARG_NONE:
		break;
	case ARG_INT:
		printf("%d", command->integer);
		break;
	case ARG_DOUBLE:
		printf("%g", command->d);
		break;
	case ARG_STRING:
		printf("%s", command->string);
		break;
	case ARG_PAGE:
		print_page(command->page);
		break;
	case ARG_POINT:
		printf("%g,%g", command->point->x, command->point->y);
		break;
	case ARG_COLOR:
		printf("%g,%g,%g,%g",
			command->color->red, command->color->green,
			command->color->blue, command->color->alpha);
		break;
	case ARG_RECTANGLE:
		printf("[%g,%g+%g,%g]",
			command->rectangle->x, command->rectangle->y,
			command->rectangle->height, command->rectangle->width);
		break;
	}
	printf("%s\n", note != NULL ? note : "");
}
void print_command_gwrap(gpointer commands, gpointer label) {
	print_command((struct cairo_command *) commands, (char *) label, NULL);
}

/*
 * print a command and current range membership
 */
void log_command(struct cairo_command *command,
		gboolean inrange, gboolean wasinrange, gboolean last,
		char *note) {
	if (! (verbose & VERBOSE_COMMAND))
		return;
	if (verbose & VERBOSE_EXECUTED) {
		if (inrange)
			print_command(command, "       ", "");
		return;
	}
	if (command->type == command_page && ! wasinrange && inrange)
		printf("        %%%%%%%%%%%%%%%%%%%%%%%%%%\n");
	print_command(command, inrange ? "       " : "        %",
		note != NULL ? note :
			command->type != command_page ? "" :
			inrange ? "      *** match" : "    --- nomatch");
	if (command->type == command_page && wasinrange && ! inrange)
		printf("        %%%%%%%%%%%%%%%%%%%%%%%%%%\n");
	if (last && ! inrange)
		printf("        %%%%%%%%%%%%%%%%%%%%%%%%%%\n");
}

/*
 * parse a special argument; accept leading and trailing spaces
 */
int argument_parse(char *argument, char *special) {
	char *scan;
	for (scan = argument; scan != NULL && isspace(*scan); scan++) {
	}
	if (scan == NULL)
		return 0;
	if (! ! memcmp(scan, special, strlen(special)))
		return 0;
	for (scan += strlen(special); *scan != '\0'; scan++)
		if (! isblank(*scan))
			return 0;
	return 1;
}

/*
 * parse a pair of numbers
 */
struct cairo_point *pair_parse(char *s) {
	gdouble x, y;
	struct cairo_point *point;

	if (sscanf(s, "[%lg,%lg]", &x, &y) != 2)
		return NULL;
	point = malloc(sizeof(struct cairo_point));
	point->x = x;
	point->y = y;
	return point;
}

/*
 * parse a page size
 */
struct cairo_point *page_parse(char *s) {
	int i;
	struct cairo_point *point;

	for (i = 0; pagesize[i].name != NULL; i++)
		if (! strcmp(s, pagesize[i].name)) {
			point = malloc(sizeof(struct cairo_point));
			point->x = pagesize[i].x;
			point->y = pagesize[i].y;
			return point;
		}
	return NULL;
}


/*
 * parse a point
 */
struct cairo_point *point_parse(char *s) {
	struct cairo_point *point;

	if ((point = page_parse(s)))
		return point;

	return pair_parse(s);
}

/*
 * parse a color
 */
struct cairo_color *color_parse(char *s) {
	gdouble red, green, blue, alpha;
	struct cairo_color *col;
	int i, n;

	for (i = 0; color[i].name != NULL; i++)
		if (! strcmp(s, color[i].name)) {
			red = color[i].red;
			green = color[i].green;
			blue = color[i].blue;
			alpha = 1.0;
			break;
		}
	if (color[i].name == NULL) {
		n = sscanf(s, "[%lg,%lg,%lg,%lg]",
			&red, &green, &blue, &alpha);
		if (n == 3)
			alpha = 0;
		else if (n < 3)
			return NULL;
	}

	if (red == RANDOM)
		red = ((gdouble) random()) / RAND_MAX * 0.8;
	if (green == RANDOM)
		green = ((gdouble) random()) / RAND_MAX * 0.8;
	if (blue == RANDOM)
		blue = ((gdouble) random()) / RAND_MAX * 0.8;

	col = malloc(sizeof(struct cairo_color));
	col->red = red;
	col->green = green;
	col->blue = blue;
	col->alpha = alpha;
	return col;
}

/*
 * parse a rectangle
 */
struct cairo_rectangle *rectangle_parse(char *s) {
	gdouble x, y, width, height, xf, yf;
	struct cairo_rectangle *rect;

	if (argument_parse(s, "pagebox")) {
		rect = malloc(sizeof(struct cairo_rectangle));
		rect->x = 0;
		rect->y = 0;
		rect->width = -1000;
		rect->height = 1;
		return rect;
	}

	if (sscanf(s, "[%lg,%lg+%lg,%lg]", &x, &y, &width, &height) == 4) {
		if (width < 0 || height < 0)
			return NULL;
		rect = malloc(sizeof(struct cairo_rectangle));
		rect->x = x;
		rect->y = y;
		rect->width = width;
		rect->height = height;
		return rect;
	}

	if (sscanf(s, "[%lg,%lg-%lg,%lg]", &x, &y, &xf, &yf) == 4) {
		rect = malloc(sizeof(struct cairo_rectangle));
		rect->x = x < xf || xf < 0 ? x : xf;
		rect->y = y < yf || yf < 0 ? y : yf;
		rect->width = xf < 0 ? xf - x : fabs(xf - x);
		rect->height = yf < 0 ? yf - y : fabs(yf - y);
		return rect;
	}

	return NULL;
}

/*
 * parsing variables
 */
struct parsing {
	int usage;
	int opterror;
	int paper;
	int count;
	int hasend;
	int level;
	char *filename;
};

/*
 * forward declaration
 */
int file_parse(GList **commands, char *filename, struct parsing *parsing);

/*
 * cut tail of string
 */
int cut_tail(char *string, char end) {
	char *tail;
	tail = strchr(string, end);
	if (tail == NULL)
		return 0;
	*tail = '\0';
	return 1;
}

/*
 * parse a command with a possible argument
 */
int command_parse(GList **commands, char *cmd, char *arg,
		struct parsing *parsing) {
	struct cairo_command *c;
	gdouble d;
	int pagenum;
	int res = 1;

	if (! strcmp(cmd, "count"))
		parsing->count = TRUE;
	else if (! strcmp(cmd, "paper"))
		parsing->paper = TRUE;
	else if (! strcmp(cmd, "help")) {
		parsing->usage = TRUE;
		parsing->opterror = FALSE;
	}
	else if (! strcmp(cmd, "output")) {
		COMMANDARG("output", "file name");
		parsing->filename = arg;
	}
	else if (! strcmp(cmd, "nooutput")) {
		parsing->filename = NULL;
	}
	else if (! strcmp(cmd, "nop")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_nop;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "file")) {
		COMMANDARG("file", "file");
		file_parse(commands, arg, parsing);
		res = 2;
	}
	else if (! strcmp(cmd, "size")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_size;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "cropbox")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_cropbox;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "verbose")) {
		COMMANDARG("verbose", "level");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_verbose;
		c->arg = ARG_INT;
		if (! strcmp(arg, "none"))
			c->integer = VERBOSE_NONE;
		else if (! strcmp(arg, "executed"))
			c->integer = VERBOSE_COMMAND | VERBOSE_EXECUTED;
		else if (! strcmp(arg, "command"))
			c->integer = VERBOSE_COMMAND;
		else {
			printf("unrecognized verbose level: %s\n", arg);
			COMMANDERROR
		}
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "page")) {
		COMMANDARG("page", "page");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_page;
		c->arg = ARG_PAGE;
		c->page = malloc(sizeof(struct cairo_page));
		if (argument_parse(arg, "all"))
			c->page->number = page_all;
		else if (argument_parse(arg, "even"))
			c->page->number = page_even;
		else if (argument_parse(arg, "odd"))
			c->page->number = page_odd;
		else if (argument_parse(arg, "last")) {
			c->page->number = page_last;
			c->page->diff = 0;
		}
		else if (sscanf(arg, "last%d", &pagenum) == 1) {
			c->page->number = page_last;
			c->page->diff = pagenum;
		}
		else
			c->page->number = atoi(arg) - 1;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "start")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_page;
		c->arg = ARG_PAGE;
		c->page = malloc(sizeof(struct cairo_page));
		c->page->number = 0;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "next")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_page;
		c->arg = ARG_PAGE;
		c->page = malloc(sizeof(struct cairo_page));
		c->page->number = page_next;
		c->page->actual = -1;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "pagerotate")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_pagerotate;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "pagesize")) {
		COMMANDARG("pagesize", "point");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_pagesize;
		c->arg = ARG_POINT;
		c->point = point_parse(arg);
		if (c->point == NULL) {
			printf("error parsing point: %s\n", arg);
			parsing->usage = TRUE;
			parsing->opterror = TRUE;
		}
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "emptypage")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_emptypage;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "copy")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_copypage;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "end")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_end;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
		parsing->hasend = TRUE;
	}
	else if (! strcmp(cmd, "discard")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_discard;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "inputpage")) {
		COMMANDARG("inputpage", "page");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_inputpage;
		c->arg = ARG_PAGE;
		c->page = malloc(sizeof(struct cairo_page));
		if (argument_parse(arg, "last")) {
			c->page->number = page_last;
			c->page->diff = 0;
		}
		else if (1 == sscanf(arg, "last-%d\n", &pagenum)) {
			c->page->number = page_last;
			c->page->diff = -pagenum;
		}
		else if (1 == sscanf(arg, "+%d\n", &pagenum)) {
			c->page->number = page_current;
			c->page->diff = pagenum;
		}
		else if (1 == sscanf(arg, "-%d\n", &pagenum)) {
			c->page->number = page_current;
			c->page->diff = -pagenum;
		}
		else
			c->page->number = atoi(arg);
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "color")) {
		COMMANDARG("color", "color");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_color;
		c->arg = ARG_COLOR;
		c->color = color_parse(arg);
		if (c->color == NULL) {
			printf("error parsing color: %s\n", arg);
			parsing->usage = TRUE;
			parsing->opterror = TRUE;
		}
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "reset")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_reset;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "rotate")) {
		COMMANDARG("rotate", "angle");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_rotate;
		c->arg = ARG_INT;
		c->integer = atoi(arg);
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "scale")) {
		COMMANDARG("scale", "factor");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_scale;
		c->arg = ARG_DOUBLE;
		c->point = pair_parse(arg);
		if (c->point == NULL) {
			d = argument_parse(arg, "sqrt2") ? M_SQRT2 :
			    argument_parse(arg, "1/sqrt2") ? M_SQRT1_2 :
				atof(arg);
			c->point = malloc(sizeof(struct cairo_point));
			c->point->x = d;
			c->point->y = d;
		}
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "clip")) {
		COMMANDARG("clip", "rectangle");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_clip;
		c->arg = ARG_RECTANGLE;
		c->rectangle = rectangle_parse(arg);
		if (c->rectangle == NULL) {
			printf("error parsing rectangle: %s\n", arg);
			parsing->usage = TRUE;
			parsing->opterror = TRUE;
		}
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "linewidth")) {
		COMMANDARG("linewidth", "width");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_linewidth;
		c->arg = ARG_DOUBLE;
		c->d = atof(arg);
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "moveto")) {
		COMMANDARG("moveto", "point");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_moveto;
		c->arg = ARG_POINT;
		c->point = point_parse(arg);
		if (c->point == NULL) {
			printf("error parsing point: %s\n", arg);
			parsing->usage = TRUE;
			parsing->opterror = TRUE;
		}
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "fontsize")) {
		COMMANDARG("fontsize", "size");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_fontsize;
		c->arg = ARG_INT;
		c->integer = atoi(arg);
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "fontface")) {
		COMMANDARG("fontface", "face");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_fontface;
		c->arg = ARG_STRING;
		c->string = arg;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "fontslant")) {
		COMMANDARG("fontslant", "slant");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_fontslant;
		c->arg = ARG_INT;
		if (argument_parse(arg, "normal"))
			c->integer = CAIRO_FONT_SLANT_NORMAL;
		else if (argument_parse(arg, "oblique"))
			c->integer = CAIRO_FONT_SLANT_OBLIQUE;
		else if (argument_parse(arg, "italic"))
			c->integer = CAIRO_FONT_SLANT_ITALIC;
		else {
			printf("error: unknown slant %s\n", arg);
			parsing->usage = TRUE;
			parsing->opterror = TRUE;
		}
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "fontweight")) {
		COMMANDARG("fontweight", "weight");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_fontweight;
		c->arg = ARG_INT;
		if (argument_parse(arg, "normal"))
			c->integer = CAIRO_FONT_WEIGHT_NORMAL;
		else if (argument_parse(arg, "bold"))
			c->integer = CAIRO_FONT_WEIGHT_BOLD;
		else {
			printf("error: unknown weight %s\n", arg);
			parsing->usage = TRUE;
			parsing->opterror = TRUE;
		}
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "box") ||
		 ! strcmp(cmd, "rectangle") ||
		 ! strcmp(cmd, "filledbox") ||
		 ! strcmp(cmd, "filledrectangle")) {
		COMMANDARG("box", "rectangle");
		c = malloc(sizeof(struct cairo_command));
		c->type = ! strcmp(cmd, "box") ||
		          ! strcmp(cmd, "rectangle") ?
				command_rectangle :
				command_filledrectangle;
		c->arg = ARG_RECTANGLE;
		c->rectangle = rectangle_parse(arg);
		if (c->rectangle == NULL) {
			printf("error parsing rectangle: %s\n", arg);
			parsing->usage = TRUE;
			parsing->opterror = TRUE;
		}
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "image")) {
		COMMANDARG("image", "string");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_image;
		c->arg = ARG_STRING;
		c->string = arg;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "extents")) {
		COMMANDARG("extents", "string");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_extents;
		c->arg = ARG_STRING;
		c->string = arg;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "print")) {
		COMMANDARG("print", "string");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_print;
		c->arg = ARG_STRING;
		c->string = arg;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "printfile")) {
		COMMANDARG("printfile", "file");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_printfile;
		c->arg = ARG_STRING;
		c->string = arg;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "link")) {
		COMMANDARG("link", "name");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_link;
		c->arg = ARG_STRING;
		c->string = arg;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "destination")) {
		COMMANDARG("destination", "name");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_destination;
		c->arg = ARG_STRING;
		c->string = arg;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "stdout")) {
		COMMANDARG("stdout", "string");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_stdout;
		c->arg = ARG_STRING;
		c->string = arg;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "comment")) {
		COMMANDARG("stdout", "string");
		c = malloc(sizeof(struct cairo_command));
		c->type = command_comment;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "paste")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_paste;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "pastesame")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_pastesame;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "pasteend")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_pasteend;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
	}
	else if (! strcmp(cmd, "stroke")) {
		c = malloc(sizeof(struct cairo_command));
		c->type = command_stroke;
		c->arg = ARG_NONE;
		*commands = g_list_append(*commands, c);
	}
	else {
		printf("unknown cmd: %s\n", cmd);
		parsing->usage = TRUE;
		parsing->opterror = TRUE;
	}
	if (verbose & VERBOSE_PARSE)
		print_command(c, "parsed", "");
	return res;
}

/*
 * parse a line into "command argument"
 */
int line_parse(GList **commands, char *line, struct parsing *parsing) {
	char *cmd, *arg;
	int pos, res;

	cut_tail(line, '\n');
	cut_tail(line, '#');
	res = sscanf(line, "%ms%n", &cmd, &pos);
	if (res <= 0 || cmd == NULL)
		return -1;
	arg = line + pos + 1;
	return command_parse(commands, cmd, arg, parsing);
}

/*
 * read commands from file
 */
int file_parse(GList **commands, char *filename, struct parsing *parsing) {
	FILE *in;
	char *line;
	size_t size;

	parsing->level++;
	if (parsing->level > 20) {
		printf("more than 20 levels of file nesting: ");
		printf("possible file loop\n");
		return -1;
	}

	printf("reading file: %s\n", filename);

	in = fopen(filename, "r");
	if (in == NULL) {
		perror(filename);
		return -1;
	}

	for (line = NULL, size = 0;
	     -1 != getline(&line, &size, in);
	     line = NULL, size = 0)
		if (0 == line_parse(commands, line, parsing))
			return 0;

	fclose(in);
	return 1;
}

/*
 * print a string
 */
int printstring(struct output *out, char *string) {
	double x, y;
	char pagenumber[100];
	cairo_font_extents_t fextents;

	cairo_font_extents(out->cr, &fextents);

	if (argument_parse(string, "%newline"))
		cairo_move_to(out->cr,
			out->startlinex,
			out->startliney + fextents.height);
	cairo_get_current_point(out->cr, &x, &y);
	if (x != out->currentlinex || y != out->currentliney) {
		/* current point changed since last print = new line started */
		out->startlinex = x;
		out->startliney = y;
	}

	if (string[0] != '%')
		cairo_show_text(out->cr, string);
	else if (string[1] == '%')
		cairo_show_text(out->cr, string + 1);
	else if (argument_parse(string, "%pagenumber")) {
		sprintf(pagenumber, "%d", out->outpage + 1);
		cairo_show_text(out->cr, pagenumber);
	}
	else if (argument_parse(string, "%newline")) {
	}
	else {
		printf("unrecognized: %s\n", string);
		exit(EXIT_FAILURE);
	}

	cairo_get_current_point(out->cr, &x, &y);
	out->currentlinex = x;
	out->currentliney = y;
	return 0;
}

/*
 * print a file
 */
int printfile(struct output out, char *filename) {
	FILE *in;
	char *line;
	size_t size;
	cairo_font_extents_t fextents;

	cairo_get_current_point(out.cr, &out.startlinex, &out.startliney);
	cairo_font_extents(out.cr, &fextents);

	in = fopen(filename, "r");
	if (in == NULL) {
		perror(filename);
		return -1;
	}

	for (line = NULL, size = 0;
	     -1 != getline(&line, &size, in);
	     line = NULL, size = 0) {
		cut_tail(line, '\n');
		cairo_show_text(out.cr, line);
		out.startliney += fextents.height;
		cairo_move_to(out.cr, out.startlinex, out.startliney);
		free(line);
	}

	fclose(in);
	cairo_get_current_point(out.cr, &out.currentlinex, &out.currentliney);
	return 0;
}

/*
 * ensure that the input file is open
 */
void ensureinput(struct input *in, struct output *out) {
	char *uri;

	if (in->doc != NULL)
		return;

	printf("    open input %s\n", in->filename);
	uri = filenametouri(in->filename);
	in->doc = poppler_document_new_from_file(uri, NULL, NULL);
	free(uri);
	if (in->doc == NULL) {
		perror(in->filename);
		exit(EXIT_FAILURE);
	}

	in->intot = poppler_document_get_n_pages(in->doc);
	if (in->intot < 1) {
		printf("no page in document\n");
		exit(EXIT_FAILURE);
	}
	out->outtot += in->intot;
}

/*
 * ensure that the current input page is read
 */
void ensurereadpage(struct input *in, struct output *out) {

				/* open input if not already */

	ensureinput(in, out);

				/* input page already open */

	if (in->page != NULL) {
		if (in->loaded == in->inpage)
			in->inpage++;
		return;
	}

				/* read the next page */

	if (in->page != NULL)
		g_object_unref(in->page);
	printf("    read page %d\n", in->inpage + 1);
	if (in->inpage >= in->intot) {
		in->page = NULL;
		in->end = TRUE;
		return;
	}
	in->page = poppler_document_get_page(in->doc, in->inpage);
	poppler_page_get_size(in->page, &in->width, &in->height);
	in->loaded = in->inpage;
	in->inpage++;
}

/*
 * close the current input page, if any
 */
void closepage(struct input *in) {
	if (in->page == NULL)
		return;
	g_object_unref(in->page);
	in->page = NULL;
}

/*
 * ensure the output page is available
 */
void ensureoutput(struct input *in, struct output *out) {

				/* output page already created */

	if (out->cr != NULL)
		return;

				/* read the input page, if needed */

	if (! out->emptypage || out->width == -1 || out->height == -1) {
		ensurereadpage(in, out);
		if (in->end) {
			out->end = TRUE;
			return;
		}
		in->inpage = in->loaded;
		if (out->rotate == 0) {
			out->width = in->width;
			out->height = in->height;
		}
		else {
			out->width = in->height;
			out->height = in->width;
		}
	}

				/* create the output page */

	if (out->surface == NULL) {
		printf("    open output %s\n", out->filename);
		out->surface = cairo_pdf_surface_create(out->filename,
			out->width, out->height);
	}

	cairo_pdf_surface_set_size(out->surface, out->width, out->height);
	out->cr = cairo_create(out->surface);

				/* copy the input page, if needed */

	if (out->emptypage)
		printf("    empty page\n");
	else {
		printf("    copy page %d\n", in->inpage + 1);
		poppler_page_render_for_printing(in->page, out->cr);
		cairo_stroke(out->cr);
		in->inpage++;
	}

				/* set font */

	cairo_select_font_face(out->cr,
		out->fontface, out->fontslant, out->fontweight);
	cairo_set_font_size(out->cr, out->fontsize);

				/* color and position */
				/* allow some initial drawing and printing */

	cairo_set_source_rgb(out->cr, 0.0, 0.0, 0.0);
	cairo_move_to(out->cr, 20.0, 20.0);
	cairo_get_current_point(out->cr, &out->startlinex, &out->startliney);
	cairo_get_current_point(out->cr,
		&out->currentlinex, &out->currentliney);
}

/*
 * fix last(-n) pages according to how many input pages are in this output page
 */
void pushend(GList *commands, int outtot, int outpage, int consume) {
	GList *scan;
	struct cairo_command *command;

	if (consume - 1 == 0)
		return;
	printf("    pushend:");

	for (scan = commands; scan != NULL; scan = scan->next) {
		command = (struct cairo_command*) scan->data;
		if (command->type != command_page)
			continue;
		if (command->page->number != page_last)
			continue;
		if (command->page->diff > 0)
			continue;
		if (outtot - 1 + command->page->diff <= outpage) {
			printf("  %d -> ", command->page->diff);
			command->page->diff += consume - 1;
			printf("%d", command->page->diff);
		}
		else if (outtot - 1 + command->page->diff > outpage &&
		         outtot - 1 + command->page->diff < outpage + consume) {
			printf("  %d => ", command->page->diff);
			command->page->diff = outpage - outtot + consume;
			printf("%d", command->page->diff);
		}
	}

	printf("\n");
}

/*
 * check whether a page is in range
 */
void pageinrange(struct cairo_command *command,
		struct input *in, struct output *out, gboolean *inrange) {
	if (command->type != command_page)
		return;

	if (command->page->number == page_all)
		*inrange = TRUE;
	else if (command->page->number == page_even)
		*inrange = (out->outpage + 1) % 2 == 0;
	else if (command->page->number == page_odd)
		*inrange = (out->outpage + 1) % 2 != 0;
	else if (command->page->number == page_current)
		printf("ERROR: page command with +n or -n\n");
	else if (command->page->number == page_last) {
		ensureinput(in, out);
		*inrange =
			out->outtot + command->page->diff - 1 == out->outpage;
	}
	else if (command->page->number == page_next)
		*inrange = command->page->actual != -1 &&
			command->page->actual == out->outpage;
	else
		*inrange = command->page->number == out->outpage;
}

/*
 * draw a rectangle
 */
void draw_rectangle(struct output *out, struct input *in,
		struct cairo_command *c) {
	double x, y, w, h;
	if (c->rectangle->width == -1000) {
		cairo_get_current_point(out->cr, &x, &y);
		cairo_translate(out->cr, x, y);
		x = 0;
		y = 0;
		w = in->width;
		h = in->height;
	}
	else {
		cairo_identity_matrix(out->cr);
		x = c->rectangle->x;
		x = x < 0 ? out->width + x : x;
		y = c->rectangle->y;
		y = y < 0 ? out->height + y : y;
		w = c->rectangle->width;
		w = w < 0 ? out->width + w : w;
		h = c->rectangle->height;
		h = h < 0 ? out->height + h : h;
	}
	cairo_rectangle(out->cr, x, y, w, h);
}

/*
 * draw an image
 */
void draw_image(struct output *out, struct cairo_command *c) {
	cairo_surface_t *insurface;
	unsigned char *data;
	unsigned long len, chunklen = 1024 * 16, r;
	FILE *infile;
	double x, y, w = 1.0, h = 1.0;

	infile = fopen(c->string, "r");
	if (infile == NULL) {
		perror(c->string);
		return;
	}
	len = 0;
	data = NULL;
	do {
		data = realloc(data, len + chunklen);
		r = fread(data + len, 1, chunklen, infile);
		len += r;
	} while (r == chunklen);
	fclose(infile);

	insurface = cairo_image_surface_create(CAIRO_FORMAT_RGB24, 1, 1);
	cairo_surface_set_mime_data(insurface, CAIRO_MIME_TYPE_JPEG,
		data, len, free, data);
	cairo_get_current_point(out->cr, &x, &y);
	cairo_set_source_surface(out->cr, insurface, x, y);
	cairo_rectangle(out->cr, x, y, 1, 1);

	cairo_user_to_device(out->cr, &x, &y);
	cairo_user_to_device(out->cr, &w, &h);
	printf("    image %s [%g,%g+%g,%g]\n", c->string, x, y, w, h);
}

/*
 * main
 */
int main(int argc, char *argv[]) {
	struct parsing parsing =
		{ FALSE, FALSE, FALSE, FALSE, FALSE, 0, "output.pdf" };
	int advance;

	struct input in;
	struct output out;

	struct cairo_command *command, *lastpage;
	GList *commands = NULL, *scan;
	PopplerRectangle cropbox;
	gdouble x, y, w, h;
	gboolean inrange, wasinrange, infocommand, drawn;
	int consume, lastpasteend;
	char *buf;
	cairo_font_extents_t fextents;
	cairo_text_extents_t textents;

				/* initalize input and output structures */

	if (argc - 1 < 1) {
		printf("file name missing\n");
		parsing.usage = TRUE;
		parsing.opterror = TRUE;
	}
	else {
		in.filename = argv[1];
		argc--;
		argv++;
	}

	in.doc = NULL;
	in.page = NULL;
	in.width = -1;
	in.height = -1;
	in.end = FALSE;
	in.loaded = -1;
	in.inpage = 0;
	in.intot = 0;

	out.surface = NULL;
	out.cr = NULL;
	out.rotate = 0;
	out.width = -1;
	out.height = -1;
	out.fontface = "mono";
	out.fontsize = 8;
	out.fontslant = CAIRO_FONT_SLANT_NORMAL;
	out.fontweight = CAIRO_FONT_WEIGHT_NORMAL;
	out.link = FALSE;
	out.end = FALSE;
	out.outtot = 0;
	out.outpage = 0;

	verbose = VERBOSE_NONE;

	srandom(time(NULL));

				/* commands */

	while (argc - 1 > 0 && advance != 0) {
		advance = command_parse(&commands, argv[1], argv[2], &parsing);
		argc -= advance;
		argv += advance;
	}
	out.filename = parsing.filename;

				/* usage */

	if (parsing.usage) {
		printf("usage:\n");
		printf("\tpdfdrawover file.pdf (command [argument])*\n");
		exit(parsing.opterror ? EXIT_FAILURE: EXIT_SUCCESS);
	}

				/* count pages */

	if (parsing.count) {
		ensureinput(&in, &out);
		printf("count: %d\n", in.intot);
		if (commands == NULL)
			return EXIT_SUCCESS;
	}

				/* paper size */

	if (parsing.paper) {
		if (findpaper(in.filename, &w, &h)) {
			printf("unknown paper: %s\n", in.filename);
			exit(EXIT_FAILURE);
		}
		printf("%gx%g\n", w, h);
		return EXIT_SUCCESS;
	}

				/* print the commands */

	if (verbose & VERBOSE_PARSE)
		g_list_foreach(commands, print_command_gwrap, "list");

				/* input and output files */

	printf("infile: %s\n", in.filename);
	printf("outfile: %s\n", out.filename);
	printf("pages:\n");

				/* page loop */

	for (out.outpage = 0, in.inpage = 0; ! in.end; out.outpage++) {
		printf("  - page: %d\n", out.outpage + 1);

					/* verbosity commands */

		inrange = TRUE;
		for (scan = commands; scan != NULL; scan = scan->next) {
			command = (struct cairo_command *) scan->data;

			pageinrange(command, &in, &out, &inrange);

			if (inrange && command->type == command_verbose)
				verbose = command->integer;
		}

					/* page creation commands */

		if (verbose & VERBOSE_COMMAND)
			printf("    page creation and setting\n");
		out.emptypage = FALSE;
		out.copypage = FALSE;
		consume = 1;		// # of input pages consumed in this
		lastpasteend = -1000;	// pages consumed at first pasteend
		lastpage = NULL;	// last page command
		inrange = TRUE;
		for (scan = commands; scan != NULL; scan = scan->next) {
			command = (struct cairo_command *) scan->data;

			wasinrange = inrange;
			pageinrange(command, &in, &out, &inrange);
			if (command->type == command_page ||
			    command->type == command_pagerotate ||
			    command->type == command_pagesize ||
			    command->type == command_emptypage ||
			    command->type == command_copypage ||
			    command->type == command_pasteend ||
			    command->type == command_pastesame ||
			    command->type == command_paste ||
			    command->type == command_end)
				log_command(command,
					inrange, wasinrange,
					scan->next == NULL, NULL);
			if (! inrange)
				continue;

			switch (command->type) {
			case command_page:
				lastpage = command;
				break;
			case command_pagerotate:
				out.rotate = 1;
				break;
			case command_pagesize:
				if (out.rotate == 0) {
					out.width = command->point->x;
					out.height = command->point->y;
				}
				else {
					out.width = command->point->y;
					out.height = command->point->x;
				}
				break;
			case command_emptypage:
				out.emptypage = TRUE;
				break;
			case command_copypage:
				out.copypage = TRUE;
				break;
			case command_pasteend:
				lastpasteend = consume + 1;
				/* fallthrough */
			case command_paste:
				consume++;
				break;
			case command_end:
				out.end = TRUE;
				break;
			default:
				;
			}
		}

					/* check result */

		if (out.emptypage) {
			consume--;
			if (lastpasteend != -1000)
				lastpasteend--;
		}
		if (lastpasteend != -1000) {
			ensureinput(&in, &out);
			printf("    lastpasteend: %d\n", lastpasteend);
			if (in.inpage + lastpasteend - 1 >= in.intot)
				out.end = TRUE;
		}

		if (out.end)
			break;
		if (out.emptypage && out.copypage) {
			printf("ERROR: both copy and emptypage ");
			printf("on page %d\n", out.outpage);
			exit(EXIT_FAILURE);
		}
		if (out.emptypage && ! parsing.hasend && consume == 0 &&
		    (lastpage == NULL || lastpage->page->number == page_all)) {
			printf("ERROR: emptypage with no page command or ");
			printf("page=all requires paste or end\n");
			exit(EXIT_FAILURE);
		}

					/* push end of output document */

		pushend(commands, out.outtot, out.outpage, consume);
		out.outtot -= consume - 1;

					/* other commands */

		if (verbose & VERBOSE_COMMAND)
			printf("    other commands\n");
		out.drawn = FALSE;
		infocommand = FALSE;
		inrange = TRUE;
		for (scan = commands; scan != NULL; scan = scan->next) {
			command = (struct cairo_command *) scan->data;

			wasinrange = inrange;
			pageinrange(command, &in, &out, &inrange);
			log_command(command, inrange, wasinrange,
				scan->next == NULL, NULL);
			if (! inrange && command->type != command_page)
				continue;

			drawn = FALSE;

			switch (command->type) {
			case command_page:
				/* concretize "next" commands in range
				 * range = the range they set
				 * wasinrange = the range they are in */
				if (command->page->number != page_next ||
				    ! wasinrange)
					break;
				command->page->actual = out.outpage + 1;
				if (verbose && VERBOSE_COMMAND)
					log_command(command, TRUE, TRUE, FALSE,
						" (set actual page number)");
				break;
			case command_copypage:
				ensureoutput(&in, &out);
				break;
			case command_discard:
				printf("    discard page %d\n", in.inpage + 1);
				in.inpage++;
				out.outtot--;
				break;
			case command_inputpage:
				ensureinput(&in, &out);
				if (command->page->number == page_last) {
					in.inpage = out.outtot - 1;
					in.inpage += command->page->diff;
				}
				else if (command->page->number == page_current)
					in.inpage += command->page->diff;
				else
					in.inpage = command->page->number - 1;
				out.outtot = out.outpage + in.intot - in.inpage;
				printf("    input page %d", in.inpage + 1);
				if (in.inpage < 0 || in.inpage >= in.intot)
					printf(" [out of range]");
				printf("\n");
				break;
			case command_size:
				ensurereadpage(&in, &out);
				if (in.end)
					break;
				if (out.emptypage) {
					printf("ERROR: size of new page\n");
					break;
				}
				printf("%d: %gx%g\n",
					in.inpage + 1, in.width, in.height);
				infocommand = TRUE;
				break;
			case command_cropbox:
				ensurereadpage(&in, &out);
				if (in.end)
					break;
				if (out.emptypage) {
					printf("ERROR: cropbox of new page\n");
					break;
				}
				poppler_page_get_crop_box(in.page, &cropbox);
				printf("%d: %g,%g-%g,%g\n", in.inpage + 1,
				       cropbox.x1, cropbox.y1,
				       cropbox.x2, cropbox.y2);
				infocommand = TRUE;
				break;
			case command_stdout:
				printf("            %s\n", command->string);
				break;
			case command_comment:
				break;
			case command_color:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				cairo_stroke(out.cr);
				cairo_set_source_rgba(out.cr,
				                     command->color->red,
				                     command->color->green,
				                     command->color->blue,
						     command->color->alpha);
				break;
			case command_reset:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				cairo_identity_matrix(out.cr);
				break;
			case command_rotate:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				cairo_identity_matrix(out.cr);
				cairo_rotate(out.cr,
					command->integer * M_PI / 180);
				break;
			case command_scale:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				cairo_scale(out.cr,
					command->point->x, command->point->y);
				break;
			case command_clip:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				if (drawn)
					cairo_stroke(out.cr);
				cairo_reset_clip(out.cr);
				draw_rectangle(&out, &in, command);
				cairo_clip(out.cr);
				break;
			case command_linewidth:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				cairo_set_line_width(out.cr, command->d);
				break;
			case command_fontsize:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				out.fontsize = command->integer;
				cairo_set_font_size(out.cr, out.fontsize);
				break;
			case command_fontface:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				out.fontface = command->string,
				cairo_select_font_face(out.cr, out.fontface,
					out.fontslant, out.fontweight);
				break;
			case command_fontslant:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				out.fontslant = command->integer,
				cairo_select_font_face(out.cr, out.fontface,
					out.fontslant, out.fontweight);
				break;
			case command_fontweight:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				out.fontweight = command->integer,
				cairo_select_font_face(out.cr, out.fontface,
					out.fontslant, out.fontweight);
				break;
			case command_moveto:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				cairo_identity_matrix(out.cr);
				x = command->point->x;
				x = x < 0 ? out.width + x : x;
				y = command->point->y;
				y = y < 0 ? out.height + y : y;
				cairo_move_to(out.cr, x, y);
				break;
			case command_rectangle:
			case command_filledrectangle:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				drawn = TRUE;
				draw_rectangle(&out, &in, command);
				if (command->type == command_filledrectangle)
					cairo_fill(out.cr);
				break;
			case command_image:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				drawn = TRUE;
				draw_image(&out, command);
				break;
			case command_extents:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				cairo_font_extents(out.cr, &fextents);
				cairo_text_extents(out.cr,
					command->string, &textents);
				printf("         %g %g %g %g\n",
					fextents.ascent, fextents.height,
					textents.x_advance, textents.y_advance);
				break;
			case command_print:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				drawn = TRUE;
				printstring(&out, command->string);
				break;
			case command_printfile:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				drawn = TRUE;
				printfile(out, command->string);
				break;
			case command_link:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				buf = malloc(strlen(command->string) + 50);
				sprintf(buf, "dest='%s'", command->string);
				cairo_tag_begin(out.cr, CAIRO_TAG_LINK, buf);
				free(buf);
				out.link = TRUE;
				break;
			case command_destination:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				cairo_get_current_point(out.cr, &x, &y);
				buf = malloc(strlen(command->string) + 50);
				sprintf(buf, "name='%s' x=%g y=%g",
					command->string, x, y);
				cairo_tag_begin(out.cr, CAIRO_TAG_DEST, buf);
				cairo_tag_end(out.cr, CAIRO_TAG_DEST);
				free(buf);
				break;
			case command_paste:
			case command_pastesame:
			case command_pasteend:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				if (in.loaded != in.inpage)
					closepage(&in);
				ensurereadpage(&in, &out);
				if (command->type == command_pastesame)
					in.inpage = in.loaded;
				if (in.end && ! out.drawn)
					out.end = TRUE;
				if (in.end)
					break;
				drawn = TRUE;
				cairo_get_current_point(out.cr, &x, &y);
				cairo_translate(out.cr, x, y);
				cairo_move_to(out.cr, 0, 0);
				poppler_page_render_for_printing(in.page,
					out.cr);
				cairo_stroke(out.cr);
				cairo_identity_matrix(out.cr);
				break;
			case command_stroke:
				ensureoutput(&in, &out);
				if (out.end)
					break;
				cairo_stroke(out.cr);
				break;
			default:
				;
			}

			if (drawn)
				out.drawn = TRUE;

			if (drawn && out.link) {
				cairo_tag_end(out.cr, CAIRO_TAG_LINK);
				out.link = FALSE;
			}

			if (out.end)
				break;
		}

		if (out.link)
			cairo_tag_end(out.cr, CAIRO_TAG_LINK);


				/* no command matched -> copy */

		if (out.cr == NULL && ! out.emptypage && ! infocommand &&
		    ! in.end && ! out.end) {
			printf("    default copy\n");
			ensureoutput(&in, &out);
		}

				/* close input and output page */

		closepage(&in);
		if (out.cr == NULL)
			continue;
		if (out.drawn)
			cairo_stroke(out.cr);
		cairo_destroy(out.cr);
		out.cr = NULL;
		if (out.end)
			break;
		printf("    show page %d\n", out.outpage + 1);
		cairo_surface_show_page(out.surface);
	}

	cairo_surface_destroy(out.surface);
	return EXIT_SUCCESS;
}

